'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('payments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      bayar_bulog: {
        type: Sequelize.STRING
      },
      tgl_bayar_bulog: {
        type: Sequelize.DATE
      },
      tagihan_bulog: {
        type: Sequelize.STRING
      },
      balance_bulog: {
        type: Sequelize.STRING
      },
      terima_bayar: {
        type: Sequelize.STRING
      },
      tgl_terima_bayar: {
        type: Sequelize.DATE
      },
      tagihan_terima: {
        type: Sequelize.STRING
      },
      balance_terima: {
        type: Sequelize.STRING
      },
      order_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('payments');
  }
};