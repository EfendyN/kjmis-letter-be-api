"use strict";
module.exports = (sequelize, DataTypes) => {
  const order = sequelize.define(
    "order",
    {
      asal_pesanan: DataTypes.STRING,
      tgl_surat_pesanan: DataTypes.DATE,
      no_surat_pesanan: DataTypes.STRING,
      tujuan_provinsi: DataTypes.STRING,
      no_surat_tujuan: DataTypes.STRING,
      tgl_surat_tujuan: DataTypes.DATE,
      daerah_tujuan: DataTypes.STRING,
      tgl_pengambilan: DataTypes.DATE,
      list_barang_id: DataTypes.INTEGER,
      payment_id: DataTypes.INTEGER,
      status: DataTypes.ENUM("Lunas", "Tunggakan"),
    },
    {}
  );
  order.associate = function (models) {
    // associations can be defined here
    order.belongsTo(models.order, {
      as: "payment",
      foreignKey: "payment_id",
    });
  };
  return order;
};
