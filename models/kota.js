"use strict";
module.exports = (sequelize, DataTypes) => {
  const kota = sequelize.define(
    "kota",
    {
      nama: DataTypes.STRING,
      provinsi_id: DataTypes.INTEGER,
    },
    {}
  );
  kota.associate = function (models) {
    // associations can be defined here
    kota.belongsTo(models.provinsi, {
      as: "provinsi",
      foreignKey: "provinsi_id",
    });
  };
  return kota;
};
