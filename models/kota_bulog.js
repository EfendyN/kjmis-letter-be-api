'use strict';
module.exports = (sequelize, DataTypes) => {
  const kota_bulog = sequelize.define('kota_bulog', {
    nama: DataTypes.STRING,
    provinsi_id: DataTypes.INTEGER
  }, {});
  kota_bulog.associate = function(models) {
    // associations can be defined here
    kota_bulog.belongsTo(models.provinsi, {
      as: "provinsi",
      foreignKey: "provinsi_id",
    });
  };
  return kota_bulog;
};