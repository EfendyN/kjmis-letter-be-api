"use strict";
module.exports = (sequelize, DataTypes) => {
  const payment = sequelize.define(
    "payment",
    {
      bayar_bulog: DataTypes.STRING,
      tgl_bayar_bulog: DataTypes.DATE,
      tagihan_bulog: DataTypes.STRING,
      balance_bulog: DataTypes.STRING,
      terima_bayar: DataTypes.STRING,
      tgl_terima_bayar: DataTypes.DATE,
      tagihan_terima: DataTypes.STRING,
      balance_terima: DataTypes.STRING,
      order_id: DataTypes.INTEGER,
    },
    {}
  );
  payment.associate = function (models) {
    // associations can be defined here
    payment.belongsTo(models.order, {
      as: "order",
      foreignKey: "order_id",
    });
  };
  return payment;
};
