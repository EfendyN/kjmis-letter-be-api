'use strict';
module.exports = (sequelize, DataTypes) => {
  const provinsi = sequelize.define('provinsi', {
    nama: DataTypes.STRING
  }, {});
  provinsi.associate = function(models) {
    // associations can be defined here
  };
  return provinsi;
};