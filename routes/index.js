const express = require("express");
const router = express.Router();
const {
  getKotaIndex,
  getProvinsiIndex,
  getOrderIndex,
  getOrderDetail,
  getKotaBulog,
} = require("../controllers/get");

const {
  postOrder,
  postOrderIndex,
  postRincianOrder,
} = require("../controllers/post");

router.get("/", (req, res) => {
  res.send("KJMIS Letter & Form API");
});

//Provinsi & Kota
router.get("/provinsi", getProvinsiIndex);
router.post("/kota", getKotaIndex);
router.get("/bulog", getKotaBulog);

//Order
router.post("/orders", postOrderIndex);
router.get("/order", getOrderDetail);
router.post("/order", postOrder);
router.post("/rincian", postRincianOrder);

module.exports = router;
