"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "payments",
      [
        {
          bayar_bulog: 2100000,
          tgl_bayar_bulog: new Date(),
          tagihan_bulog: 2100000,
          balance_bulog: 0,
          terima_bayar: 2100000,
          tgl_terima_bayar: new Date(),
          tagihan_terima: 2100000,
          balance_terima: 0,
          order_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("payments", null, {});
  },
};
