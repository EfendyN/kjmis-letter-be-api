"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "kota",
      [
        {
          nama: "ACEH BARAT",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH BARAT DAYA",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH BESAR",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH JAYA",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH SELATAN",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH SINGKIL",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH TAMIANG",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH TENGAH",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH TENGGARA",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH TIMUR",
          provinsi_id: 1, // aceh
        },
        {
          nama: "ACEH UTARA",
          provinsi_id: 1, // aceh
        },
        {
          nama: "BENER MERIAH",
          provinsi_id: 1, // aceh
        },
        {
          nama: "BIREUEN",
          provinsi_id: 1, // aceh
        },
        {
          nama: "GAYO LUES",
          provinsi_id: 1, // aceh
        },
        {
          nama: "KOTA BANDA ACEH",
          provinsi_id: 1, // aceh
        },
        {
          nama: "KOTA LANGSA",
          provinsi_id: 1, // aceh
        },
        {
          nama: "KOTA LHOKSEUMAWE",
          provinsi_id: 1, // aceh
        },
        {
          nama: "KOTA SABANG",
          provinsi_id: 1, // aceh
        },
        {
          nama: "KOTA SUBULUSSALAM",
          provinsi_id: 1, // aceh
        },
        {
          nama: "NAGAN RAYA",
          provinsi_id: 1, // aceh
        },
        {
          nama: "Pprovinsi_IDIE",
          provinsi_id: 1, // aceh
        },
        {
          nama: "Pprovinsi_IDIE JAYA",
          provinsi_id: 1, // aceh
        },
        {
          nama: "SIMEULUE",
          provinsi_id: 1, // aceh
        },
        {
          nama: "BADUNG",
          provinsi_id: 2, // bali
        },
        {
          nama: "BANGLI",
          provinsi_id: 2, // bali
        },
        {
          nama: "BULELENG",
          provinsi_id: 2, // bali
        },
        {
          nama: "GIANYAR",
          provinsi_id: 2, // bali
        },
        {
          nama: "JEMBRANA",
          provinsi_id: 2, // bali
        },
        {
          nama: "KARANG ASEM",
          provinsi_id: 2, // bali
        },
        {
          nama: "KLUNGKUNG",
          provinsi_id: 2, // bali
        },
        {
          nama: "KOTA DENPASAR",
          provinsi_id: 2, // bali
        },
        {
          nama: "TABANAN",
          provinsi_id: 2, // bali
        },
        {
          nama: "KOTA CILEGON",
          provinsi_id: 3, // banten
        },
        {
          nama: "KOTA SERANG",
          provinsi_id: 3, // banten
        },
        {
          nama: "KOTA TANGERANG",
          provinsi_id: 3, // banten
        },
        {
          nama: "KOTA TANGERANG SELATAN",
          provinsi_id: 3, // banten
        },
        {
          nama: "LEBAK",
          provinsi_id: 3, // banten
        },
        {
          nama: "PANDEGLANG",
          provinsi_id: 3, // banten
        },
        {
          nama: "SERANG",
          provinsi_id: 3, // banten
        },
        {
          nama: "TANGERANG",
          provinsi_id: 3, // banten
        },
        {
          nama: "BENGKULU SELATAN",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "BENGKULU TENGAH",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "BENGKULU UTARA",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "KAUR",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "KEPAHIANG",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "KOTA BENGKULU",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "LEBONG",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "MUKOMUKO",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "REJANG LEBONG",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "SELUMA",
          provinsi_id: 4, // bengkulu
        },
        {
          nama: "BANTUL",
          provinsi_id: 5, // DI Yograkarta
        },
        {
          nama: "GUNUNG Kprovinsi_IDUL",
          provinsi_id: 5, // DI Yograkarta
        },
        {
          nama: "KOTA YOGYAKARTA",
          provinsi_id: 5, // DI Yograkarta
        },
        {
          nama: "KULON PROGO",
          provinsi_id: 5, // DI Yograkarta
        },
        {
          nama: "SLEMAN",
          provinsi_id: 5, // DI Yograkarta
        },
        {
          nama: "KEPULAUAN SERIBU",
          provinsi_id: 6, // dki jakarta
        },
        {
          nama: "KOTA JAKARTA BARAT",
          provinsi_id: 6, // dki jakarta
        },
        {
          nama: "KOTA JAKARTA PUSAT",
          provinsi_id: 6, // dki jakarta
        },
        {
          nama: "KOTA JAKARTA SELATAN",
          provinsi_id: 6, // dki jakarta
        },
        {
          nama: "KOTA JAKARTA TIMUR",
          provinsi_id: 6, // dki jakarta
        },
        {
          nama: "KOTA JAKARTA UTARA",
          provinsi_id: 6, // dki jakarta
        },
        {
          nama: "BOALEMO",
          provinsi_id: 7, // gorontalo
        },
        {
          nama: "BONE BOLANGO",
          provinsi_id: 7, // gorontalo
        },
        {
          nama: "GORONTALO",
          provinsi_id: 7, // gorontalo
        },
        {
          nama: "GORONTALO UTARA",
          provinsi_id: 7, // gorontalo
        },
        {
          nama: "KOTA GORONTALO",
          provinsi_id: 7, // gorontalo
        },
        {
          nama: "POHUWATO",
          provinsi_id: 7, // gorontalo
        },
        {
          nama: "BATANG HARI",
          provinsi_id: 8, //jambi
        },
        {
          nama: "BUNGO",
          provinsi_id: 8, //jambi
        },
        {
          nama: "KERINCI",
          provinsi_id: 8, //jambi
        },
        {
          nama: "KOTA JAMBI",
          provinsi_id: 8, //jambi
        },
        {
          nama: "KOTA SUNGAI PENUH",
          provinsi_id: 8, //jambi
        },
        {
          nama: "MERANGIN",
          provinsi_id: 8, //jambi
        },
        {
          nama: "MUARO JAMBI",
          provinsi_id: 8, //jambi
        },
        {
          nama: "SAROLANGUN",
          provinsi_id: 8, //jambi
        },
        {
          nama: "TANJUNG JABUNG BARAT",
          provinsi_id: 8, //jambi
        },
        {
          nama: "TANJUNG JABUNG TIMUR",
          provinsi_id: 8, //jambi
        },
        {
          nama: "TEBO",
          provinsi_id: 8, //jambi
        },
        {
          nama: "BANDUNG",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "BANDUNG BARAT",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "BEKASI",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "BOGOR",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "CIAMIS",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "CIANJUR",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "CIREBON",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "GARUT",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "INDRAMAYU",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KARAWANG",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA BANDUNG",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA BANJAR",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA BEKASI",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA BOGOR",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA CIMAHI",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA CIREBON",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA DEPOK",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA SUKABUMI",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KOTA TASIKMALAYA",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "KUNINGAN",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "MAJALENGKA",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "PANGANDARAN",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "PURWAKARTA",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "SUBANG",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "SUKABUMI",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "SUMEDANG",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "TASIKMALAYA",
          provinsi_id: 9, // jawa barat
        },
        {
          nama: "BANJARNEGARA",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "BANYUMAS",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "BATANG",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "BLORA",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "BOYOLALI",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "BREBES",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "CILACAP",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "DEMAK",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "GROBOGAN",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "JEPARA",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KARANGANYAR",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KEBUMEN",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KENDAL",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KLATEN",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KOTA MAGELANG",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KOTA PEKALONGAN",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KOTA SALATIGA",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KOTA SEMARANG",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KOTA SURAKARTA",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KOTA TEGAL",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "KUDUS",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "MAGELANG",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "PATI",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "PEKALONGAN",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "PEMALANG",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "PURBALINGGA",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "PURWOREJO",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "REMBANG",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "SEMARANG",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "SRAGEN",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "SUKOHARJO",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "TEGAL",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "TEMANGGUNG",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "WONOGIRI",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "WONOSOBO",
          provinsi_id: 10, //jawa tengah
        },
        {
          nama: "BANGKALAN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "BANYUWANGI",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "BLITAR",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "BOJONEGORO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "BONDOWOSO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "GRESIK",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "JEMBER",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "JOMBANG",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KEDIRI",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA BATU",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA BLITAR",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA KEDIRI",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA MADIUN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA MALANG",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA MOJOKERTO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA PASURUAN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA PROBOLINGGO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "KOTA SURABAYA",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "LAMONGAN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "LUMAJANG",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "MADIUN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "MAGETAN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "MALANG",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "MOJOKERTO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "NGANJUK",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "NGAWI",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "PACITAN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "PAMEKASAN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "PASURUAN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "PONOROGO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "PROBOLINGGO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "SAMPANG",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "Sprovinsi_IDOARJO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "SITUBONDO",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "SUMENEP",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "TRENGGALEK",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "TUBAN",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "TULUNGAGUNG",
          provinsi_id: 11, // jawa timur
        },
        {
          nama: "Kota Samarinda",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kota Bontang",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kota Balikpapan",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kab. Mahakam Ulu",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kab. Penajam Paser Utara",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kab. Kutai Timur",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kab. Kutai Barat",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kab. Berau",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kab. Kutai Kertanegara",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kab. Paser",
          provinsi_id: 12, //kalimantan timur
        },
        {
          nama: "Kota Tarakan",
          provinsi_id: 13, // kalimantan utara
        },
        {
          nama: "Kab. Tana Tprovinsi_idung",
          provinsi_id: 13, // kalimantan utara
        },
        {
          nama: "Kab. Nunukan",
          provinsi_id: 13, // kalimantan utara
        },
        {
          nama: "Kab. Malinau",
          provinsi_id: 13, // kalimantan utara
        },
        {
          nama: "Kab. Bulungan",
          provinsi_id: 13, // kalimantan utara
        },
        {
          nama: "Kota Manado",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kota Tomohon",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kota Bitung",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kota Kotamobagu",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Bolaang Mangondow Selatan",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Bolaang Mangondow Timur",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Kepulauan Siau Tagulandang Biaro",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Bolaang Mangondow Utara",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Minahasa Tenggara",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Minahasa Utara",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Minahasa Selatan",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Kepulauan Talaud",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Kepulauan Sangihe",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Minahasa",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kab. Bolaang Mangondow",
          provinsi_id: 14, // sulawesi utara
        },
        {
          nama: "Kota Palu",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Morowali Utara",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Banggai Laut",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Sigi",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Tojo Una-Una",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Parigi Moutong",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Banggai Kepulauan",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Morowali",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Buol",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Toli-Toli",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Donggala",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Poso",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kab. Banggai",
          provinsi_id: 15, // sulawesi tengah
        },
        {
          nama: "Kota Makasar",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kota Palopo",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kota Pare Pare",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Toraja Utara",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Luwu Timur",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Luwu Utara",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Tana Toraja",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Luwu",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Enrekang",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Pinrang",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Sprovinsi_idenreng Rappang",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Wajo",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Soppeng",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Barru",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Pangkajene Kepulauan",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Maros",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Bone",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Sinjai",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Gowa",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Takalar",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Jeneponto",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Bantaeng",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Bulukumba",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kab. Kepulauan Selayar",
          provinsi_id: 16, // sulawesi selatan
        },
        {
          nama: "Kota Kendari",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kota Bau Bau",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Buton Selatan",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Buton Tengah",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Muna Barat",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Konawe Kepulauan",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Kolaka Timur",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Buton Utara",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Konawe Utara",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Kolaka Utara",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Wakatobi",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Bombana",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Konawe Selatan",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Buton",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Muna",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Konawe",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Kolaka",
          provinsi_id: 17, // sulawesi tenggara
        },
        {
          nama: "Kab. Majene",
          provinsi_id: 18, // sulawesi barat
        },
        {
          nama: "Kab. Polowali Mandar",
          provinsi_id: 18, // sulawesi barat
        },
        {
          nama: "Kab. Mamasa",
          provinsi_id: 18, // sulawesi barat
        },
        {
          nama: "Kab. Mamuju",
          provinsi_id: 18, // sulawesi barat
        },
        {
          nama: "Kab. Mamuju Utara",
          provinsi_id: 18, // sulawesi barat
        },
        {
          nama: "Kab. Mamuju Tengah",
          provinsi_id: 18, // sulawesi barat
        },
        {
          nama: "Kota Ambon",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kota Tual",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Buru Selatan",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Maluku Barat Daya",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Kepulauan Aru",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Seram Bagian Barat",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Seram Bagian Timur",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Buru",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Maluku Tenggara Barat",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Maluku Tenggara",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kab. Maluku Tengah",
          provinsi_id: 19, // maluku
        },
        {
          nama: "Kota Ternate",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kota Tprovinsi_idore Kepulauan",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kab. Pulau Taliabu",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kab. Pulau Morotai",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kab. Halmahera Timur",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kab. Kepulauan Sula",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kab. Halmahera Selatan",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kab. Halmahera Utara",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kab. Halmahera Tengah",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kab. Halmahera Barat",
          provinsi_id: 20, // maluku utara
        },
        {
          nama: "Kota Jayapura",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Deiyai",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Intan Jaya",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Dogiyai",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Puncak",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Nduga",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Lanny Jaya",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Yalimo",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Mamberamo Tengah",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Mamberamo Raya",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Supiori",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Asmat",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Mappi",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Boven Digoel",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Waropen",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Tolikara",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Yahukimo",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Pegunungan Bintang",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Keerom",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Sarmi",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Mimika",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Paniai",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Puncak Jaya",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Biak Numfor",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Kepulauan Yapen",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Nabire",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Jayapura",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Jayawijaya",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kab. Merauke",
          provinsi_id: 21, // papua
        },
        {
          nama: "Kota Sorong",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Pegunungan Arfak",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Manokwari Selatan",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Maybrat",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Tambrauw",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Kaimana",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Teluk Wondama",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Teluk Bintuni",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Raja Ampat",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Sorong Selatan",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Fak Fak",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Manokwari",
          provinsi_id: 22, // papua barat
        },
        {
          nama: "Kab. Sorong",
          provinsi_id: 22, // papua barat
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("kota", null, {});
  },
};
