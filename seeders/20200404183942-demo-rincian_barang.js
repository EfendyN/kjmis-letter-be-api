"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "rincian_barangs",
      [
        {
          nama_barang: "Beras",
          unit: 20,
          unit_type: "Kg",
          jumlah: 10,
          harga_perunit: 10000,
          total_harga: 2000000,
          het: 10000,
          order_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nama_barang: "Gula",
          unit: 10,
          unit_type: "Kg",
          jumlah: 10,
          harga_perunit: 1000,
          total_harga: 100000,
          het: 1000,
          order_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("rincian_barangs", null, {});
  },
};
