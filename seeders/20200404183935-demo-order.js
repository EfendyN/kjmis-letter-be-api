"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "orders",
      [
        {
          asal_pesanan: "KJMIS",
          tgl_surat_pesanan: new Date(),
          no_surat_pesanan: "../KJMIS/04/2020",
          tujuan_provinsi: "Jawa Barat",
          no_surat_tujuan: "../KJMIS/04/2020",
          tgl_surat_tujuan: new Date(),
          daerah_tujuan: "Bekasi",
          tgl_pengambilan: new Date(),
          list_barang_id: 1,
          payment_id: 1,
          status: "Lunas",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("orders", null, {});
  },
};
