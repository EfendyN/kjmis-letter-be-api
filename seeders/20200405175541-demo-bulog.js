"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "kota_bulogs",
      [
        {
          nama: "KOTA BANDA ACEH",
          provinsi_id: 1,
        },
        {
          nama: "Kota Medan",
          provinsi_id: 2,
        },
        {
          nama: "Kota Padang",
          provinsi_id: 3,
        },
        {
          nama: "Kota Pekan Baru",
          provinsi_id: 4,
        },
        {
          nama: "KOTA JAMBI",
          provinsi_id: 5,
        },
        {
          nama: "Kota Palembang",
          provinsi_id: 6,
        },
        {
          nama: "KOTA BENGKULU",
          provinsi_id: 7,
        },
        {
          nama: "Kota Bandar Lampung",
          provinsi_id: 8,
        },
        {
          nama: "Kota Pangkal Pinang",
          provinsi_id: 9,
        },
        {
          nama: "Kota Tanjung Pinang",
          provinsi_id: 10,
        },
        {
          nama: "KOTA JAKARTA PUSAT",
          provinsi_id: 11,
        },
        {
          nama: "KOTA BANDUNG",
          provinsi_id: 12,
        },
        {
          nama: "KOTA SEMARANG",
          provinsi_id: 13,
        },
        {
          nama: "KOTA YOGYAKARTA",
          provinsi_id: 14,
        },
        {
          nama: "KOTA SURABAYA",
          provinsi_id: 15,
        },
        {
          nama: "KOTA TANGERANG",
          provinsi_id: 16,
        },
        {
          nama: "KOTA DENPASAR",
          provinsi_id: 17,
        },
        {
          nama: "Kota Mataram",
          provinsi_id: 18,
        },
        {
          nama: "Kota Kupang",
          provinsi_id: 19,
        },
        {
          nama: "Kota Pontianak",
          provinsi_id: 20,
        },
        {
          nama: "Kota Palangkaraya",
          provinsi_id: 21,
        },
        {
          nama: "Kota Banjarmasin",
          provinsi_id: 22,
        },
        {
          nama: "Kota Samarinda",
          provinsi_id: 23,
        },
        {
          nama: "Kota Samarinda",
          provinsi_id: 24,
        },
        {
          nama: "Kota Manado",
          provinsi_id: 25,
        },
        {
          nama: "Kota Palu",
          provinsi_id: 26,
        },
        {
          nama: "Kota Makasar",
          provinsi_id: 27,
        },
        {
          nama: "Kota Kendari",
          provinsi_id: 28,
        },
        {
          nama: "KOTA GORONTALO",
          provinsi_id: 29,
        },
        {
          nama: "Kota Makasar",
          provinsi_id: 30,
        },
        {
          nama: "Kota Ambon",
          provinsi_id: 31,
        },
        {
          nama: "Kota Ambon",
          provinsi_id: 32,
        },
        {
          nama: "Kota Jayapura",
          provinsi_id: 33,
        },
        {
          nama: "Kota Jayapura",
          provinsi_id: 34,
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("kota_bulogs", null, {});
  },
};
