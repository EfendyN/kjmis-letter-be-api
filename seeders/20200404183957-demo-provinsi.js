"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "provinsis",
      [
        {
          nama: "ACEH",
        },
        {
          nama: "SUMATERA UTARA",
        },
        {
          nama: "SUMATERA BARAT",
        },
        {
          nama: "RIAU",
        },
        {
          nama: "JAMBI",
        },
        {
          nama: "SUMATERA SELATAN",
        },
        {
          nama: "BENGKULU",
        },
        {
          nama: "LAMPUNG",
        },
        {
          nama: "KEPULAUAN BANGKA BELITUNG",
        },
        {
          nama: "KEPULAUAN RIAU",
        },
        {
          nama: "DKI JAKARTA",
        },
        {
          nama: "JAWA BARAT",
        },
        {
          nama: "JAWA TENGAH",
        },
        {
          nama: "DI YOGYAKARTA",
        },
        {
          nama: "JAWA TIMUR",
        },
        {
          nama: "BANTEN",
        },
        {
          nama: "BALI",
        },
        {
          nama: "NUSA TENGGARA BARAT",
        },
        {
          nama: "NUSA TENGGARA TIMUR",
        },
        {
          nama: "KALIMANTAN BARAT",
        },
        {
          nama: "KALIMANTAN TENGAH",
        },
        {
          nama: "KALIMANTAN SELATAN",
        },
        {
          nama: "KALIMANTAN TIMUR",
        },
        {
          nama: "KALIMANTAN UTARA",
        },
        {
          nama: "SULAWESI UTARA",
        },
        {
          nama: "SULAWESI TENGAH",
        },
        {
          nama: "SULAWESI SELATAN",
        },
        {
          nama: "SULAWESI TENGGARA",
        },
        {
          nama: "GORONTALO",
        },
        {
          nama: "SULAWESI BARAT",
        },
        {
          nama: "MALUKU",
        },
        {
          nama: "MALUKU UTARA",
        },
        {
          nama: "PAPUA BARAT",
        },
        {
          nama: "PAPUA",
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("provinsis", null, {});
  },
};
