const models = require("../models");
const Sequelize = require("sequelize");
const { Op } = Sequelize;
const Order = models.order;
const Payment = models.payment;
const Rincian = models.rincian_barang;

exports.postOrderIndex = async (req, res) => {
  const {
    asal_pesanan,
    tgl_surat_pesanan,
    no_surat_pesanan,
    tujuan_provinsi,
    no_surat_tujuan,
    tgl_surat_tujuan,
    daerah_tujuan,
    tgl_pengambilan,
    status,
  } = req.query;
  try {
    const orders = await Order.findAll(
      {
        where: {
          asal_pesanan: { [Op.substring]: asal_pesanan },
          tgl_surat_pesanan: { [Op.substring]: tgl_surat_pesanan },
          no_surat_pesanan: { [Op.substring]: no_surat_pesanan },
          tujuan_provinsi: { [Op.substring]: tujuan_provinsi },
          no_surat_tujuan: { [Op.substring]: no_surat_tujuan },
          tgl_surat_tujuan: { [Op.substring]: tgl_surat_tujuan },
          daerah_tujuan: { [Op.substring]: daerah_tujuan },
          tgl_pengambilan: { [Op.substring]: tgl_pengambilan },
          status: { [Op.substring]: status },
        },
        order: [["id", "DESC"]],
      },
      {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      }
    );
    res.send({ data: orders });
  } catch (err) {
    console.log(err);
  }
};

exports.postRincianOrder = async (req, res) => {
  const { id } = req.query;
  try {
    const rincian = await Rincian.findAll(
      {
        where: { order_id: id },
        order: [["id", "ASC"]],
      },
      {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      }
    );
    res.send({ data: rincian });
  } catch (err) {
    console.log(err);
  }
};

exports.postOrder = async (req, res) => {
  const { order, list_rincian } = req.body;
  try {
    const createOrder = await Order.create(order);
    const createRincian = await Rincian.bulkCreate(list_rincian);
    if (createOrder) {
      const updateStatusOrder = await Order.update(
        { status: "Tunggakan" },
        { where: { id: createOrder.id } }
      );

      let result = createRincian.map(({ id }) => id);
      let result_harga = createRincian.map(({ total_harga }) => total_harga);
      let result_het = createRincian.map(({ het }) => het);

      let sum_harga = result_harga.reduce(function (a, b) {
        return a + b;
      }, 0);

      let sum_het = result_het.reduce(function (a, b) {
        return a + b;
      }, 0);

      console.log(
        { sum_harga },
        { result_harga },
        { sum_het },
        { result_het },
        "asdasdas das dasdas dasd asd"
      );

      const updateRincian = await Rincian.update(
        {
          order_id: createOrder.id,
        },
        {
          where: {
            id: { [Op.in]: result },
          },
        }
      );
      const createPayment = await Payment.create({
        tagihan_bulog: sum_harga,
        order_id: createOrder.id,
      });

      res.status(200).json({ data: { createOrder } });
    }
  } catch (err) {
    console.log(err);
  }
};
