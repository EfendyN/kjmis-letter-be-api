const models = require("../models");

const Orders = models.order;
const Payment = models.payment;
const Rincian = models.rincian_barang;

const Provinsi = models.provinsi;
const Kota = models.kota;
const Bulog = models.kota_bulog;

exports.getProvinsiIndex = async (req, res) => {
  try {
    const provinsi = await Provinsi.findAll({
      attributes: {
        exclude: [, "createdAt", "updatedAt"],
      },
    });
    res.send({ data: provinsi });
  } catch (err) {
    console.log(err);
  }
};

exports.getKotaIndex = async (req, res) => {
  const { provinsi } = req.query;
  console.log({ provinsi });
  try {
    if (provinsi === "" || provinsi === undefined) {
      const kota = await Kota.findAll({
        include: [{ model: Provinsi, as: "provinsi" }],
        attributes: {
          exclude: [, "createdAt", "updatedAt"],
        },
        order: [["nama", "ASC"]],
      });
      res.send({ data: kota });
    } else {
      const kota = await Kota.findAll(
        { where: { provinsi_id: provinsi } },
        {
          include: [{ model: Provinsi, as: "provinsi" }],
          attributes: {
            exclude: [, "createdAt", "updatedAt"],
          },
        }
      );
      res.send({ data: kota });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.getKotaBulog = async (req, res) => {
  try {
    const provinsi = await Bulog.findAll({
      include: { model: Provinsi, as: "provinsi" },
      attributes: {
        exclude: [, "createdAt", "updatedAt"],
      },
    });
    res.send({ data: provinsi });
  } catch (err) {
    console.log(err);
  }
};

exports.getOrderIndex = async (req, res) => {
  try {
    const orders = await Orders.findAll({
      attributes: {
        exclude: [, "createdAt", "updatedAt"],
      },
    });
    res.send({ data: orders });
  } catch (err) {
    console.log(err);
  }
};

exports.getOrderDetail = async (req, res) => {
  try {
    const { order_id } = req.query;
    const orderDetail = await Orders.findOne({
      where: { id: order_id },
      attributes: { exclude: ["createdAt", "updatedAt"] },
    });
    if (orderDetail === null) {
      res.status(200).send({ message: "no data found", data: orderDetail });
    } else {
      const rincian = await Rincian.findAll({
        where: { order_id },
      });
      const payment = await Payment.findAll({
        where: { order_id },
      });
      res.status(200).send({
        message: "success to find data",
        data: { order: orderDetail, rincian: rincian, payment: payment },
      });
    }
  } catch (err) {
    console.log(err);
  }
};
